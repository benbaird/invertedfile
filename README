Implementation of the Inverted File data structure in C using Binary Search Tree and Linked List. Two available modes: Online and offline.

Offline: Build a database of keywords from the datafiles and create the dictionary, posting, and docids files.

                - dictionary.txt: contains all terms in the binary search tree
                                  along with the number of documents in which they occur.
                                  Sorted alphabetically.
                    <total number of terms>
                    <term1> <document-frequency1>
                    <term2> <document-frequency2>
                      
                - postings.txt: contains the document's id and it's term frequency
                    <total number of entries>
                    <docno1> <term-frequency1>
                    <docno2> <term-frequency2>
                    
                - docids.txt: docid's with their starting positions from the input file
                    <total number of documents>
                    <docid1> <start-position1>
                    <docid2> <start-position2>

Online: Use the created files with a query to find relevant documents and return
        their titles.

User Guide:
    make : to compile the program
    ./bairdb_a4_off : Execute the offline program to process a file and generate 
                     inverted file
                     When in program enter:
                        1 : to generate the files
                            enter filename: e.g. DataFiles/full.txt
                        2 : print off the dictionary alphabetically
                        3 : print off the documents.txt
                        q : quit
    ./bairdb_a4_on : Execute the online program and input a query
                     When in program enter:
                         <query> : to search for terms using the inverted file
                         q : to quit
                     After query found:
                        a : previous 10 results
                        d : next 10 results
                        q : return to main loop
    make reset : remove posting, dictionary, docindex files
    make clean : to remove any .o files and the online/offline files after compilation
        
Limitations:
    Can only load one file at a time.
    
Improvements:
    Implement a better sorting algorithm, currently use C's quicksort.
    Convert current unbalanced binary tree to on that is more balanced, AVL or B*.
    Loading the full.txt(100mb) takes >30min, so improving that would be ideal
        - included precompiled indexes
    
Testing:
    Tested for all memory leaks and errors.
    Tested using using many prints to verify intermediary steps.
    Tested with different files of varying sizes.
    Unit tested all the supplmentary files, i.e. lists.c, trees.c, etc
    Tested most boundaries that I could think of
    Check for most errors, except for null returns on malloc
